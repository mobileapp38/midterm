import 'package:device_preview/device_preview.dart';
import 'package:flutter/material.dart';

import 'home_page.dart';

void main() {
  runApp(const RegApp());
}

class RegApp extends StatelessWidget {
  const RegApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DevicePreview(
      tools: const [
        DeviceSection(),
      ],
      builder: (context) => MaterialApp(
        debugShowCheckedModeBanner: false,
        useInheritedMediaQuery: true,
        builder: DevicePreview.appBuilder,
        locale: DevicePreview.locale(context),
        theme: ThemeData(
          primarySwatch: Colors.green,
        ),
        home: Bottom(),
        ),
      );
  }
}