import 'package:flutter/material.dart';

class Grade extends StatefulWidget {
  const Grade({Key? key}) : super(key: key);

  @override
  State<Grade> createState() => _GradeState();
}

class _GradeState extends State<Grade> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.amber[100],
      bottomNavigationBar: BottomNavigationBar(items: [
        BottomNavigationBarItem(icon: Icon(Icons.home), label: 'Home'),
        BottomNavigationBarItem(
            icon: Icon(Icons.person_pin_rounded), label: 'Grade'),
        BottomNavigationBarItem(
            icon: Icon(Icons.calendar_month), label: 'Calender'),
      ]),
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: NetworkImage(
                "https://www.facebook.com/messenger_media/?attachment_id=585490109643315&message_id=mid.%24cAABa88pSHkyMPSGTmmGC9w5yma5r&thread_id=100003117932108"),
            fit: BoxFit.cover,
          ),
        ),
        padding: EdgeInsets.all(12),
        child: ListView(
          children: <Widget>[
            Profile(),
            BioPro(),
          ],
        ),
      ),
    );
  }

  Widget Profile() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Column(
          children: [
            Icon(Icons.person_pin_rounded, size: 150, color: Colors.amber),
          ],
        ),
      ],
    );
  }

  Widget BioPro() {
    return Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
      color: Colors.white,
      child: SizedBox(
        height: 300,
        child: ListView(
          scrollDirection: Axis.horizontal,
          padding: EdgeInsets.all(25),
          children: <Widget>[
            columnBibliography(),
            columnDataBibliography(),
          ],
        ),
      ),
    );
  }

  Widget columnBibliography() {
    return Row(
      children: <Widget>[
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text("   GPA ",
                style: TextStyle(
                    height: 2,
                    color: Colors.amberAccent.shade700,
                    fontSize: 18)),
            Text("   ID :", style: TextStyle(height: 2.7, fontSize: 13)),
            Text("   NAME :", style: TextStyle(height: 2.0, fontSize: 13)),
            Text("   PROGRAM :", style: TextStyle(height: 2.0, fontSize: 13)),
            Text("   CREDIT :", style: TextStyle(height: 2.0, fontSize: 13)),
            Text("   GPA :", style: TextStyle(height: 2.0, fontSize: 13)),
          ],
        ),
      ],
    );
  }

  Widget columnDataBibliography() {
    return Row(
      children: <Widget>[
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text("", style: TextStyle(height: 2.0, fontSize: 14)),
            Text("63160065", style: TextStyle(height: 2.7, fontSize: 14)),
            Text("Keetapat Chomrasri",
                style: TextStyle(height: 2.0, fontSize: 14)),
            Text("(CS)Computer Science",
                style: TextStyle(height: 2.0, fontSize: 14)),
            Text("92",
                style: TextStyle(height: 2.0, fontSize: 14)),
            Text("2.8",
                style: TextStyle(height: 2.0, fontSize: 14)),
          ],
        ),
      ],
    );
  }
}
