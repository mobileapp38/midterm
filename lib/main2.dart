import 'package:device_preview/device_preview.dart';
import 'package:flutter/material.dart';

import 'login.dart';

void main() {
  runApp(const ColumnExample());
}

class ColumnExample extends StatelessWidget {
  const ColumnExample({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DevicePreview(
      tools: const [
        DeviceSection(),
      ],
      builder: (context) => MaterialApp(
        debugShowCheckedModeBanner: false,
        useInheritedMediaQuery: true,
        builder: DevicePreview.appBuilder,
        locale: DevicePreview.locale(context),
        title: 'Burapha App',
        theme: ThemeData(
          primarySwatch: Colors.amber,
        ),
        home: Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            title: const Text('BUU APP'),
          ),
          body: const SafeArea(
            child: LoginPage(),
          ),
        ),
      ),
    );
  }
}
