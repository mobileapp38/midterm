import 'package:flutter/material.dart';
import 'grade.dart';
// class HomePage extends StatefulWidget {
//   const HomePage({Key? key}) : super(key: key);
//
//   @override
//   State<HomePage> createState() => _HomePageState();
// }
class Bottom extends StatefulWidget {
  const Bottom({Key? key}) : super(key: key);

  @override
  State<Bottom> createState() => _BottomBar();
}
class _BottomBar extends State<Bottom> {
  int _selectedIndex = 0;
  static final List<Widget> _widgetOptions = <Widget>[
    Grade(),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.amber[100],
      bottomNavigationBar: BottomNavigationBar(
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Home',
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.person_pin_rounded),
              label: 'Grade'),
          BottomNavigationBarItem(
              icon: Icon(Icons.calendar_month), label: 'Calender'),
        ],
      ),
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: NetworkImage(
                "https://www.facebook.com/messenger_media/?attachment_id=585490109643315&message_id=mid.%24cAABa88pSHkyMPSGTmmGC9w5yma5r&thread_id=100003117932108"),
            fit: BoxFit.cover,
          ),
        ),
        padding: EdgeInsets.all(12),
        child: ListView(
          children: <Widget>[
            Profile(),
            BioPro(),
          ],
        ),
      ),
    );
  }

  Widget Profile() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Column(
          children: [
            Icon(Icons.person_pin, size: 150, color: Colors.amber),
          ],
        ),
      ],
    );
  }

  Widget BioPro() {
    return Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
      color: Colors.white,
      child: SizedBox(
        height: 300,
        child: ListView(
          scrollDirection: Axis.horizontal,
          padding: EdgeInsets.all(25),
          children: <Widget>[
            colprofile(),
            colbioprofile(),
          ],
        ),
      ),
    );
  }

  Widget colprofile() {
    return Row(
      children: <Widget>[
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text("   PROFILE  :– ) ",
                style: TextStyle(
                    height: 2,
                    color: Colors.amberAccent.shade700,
                    fontSize: 18)),
            Text(
                "   ID :", style: TextStyle(height: 2.7, fontSize: 13)),
            Text("   NAME :", style: TextStyle(height: 2.0, fontSize: 13)),
            Text("   NICKNAME :", style: TextStyle(height: 2.0, fontSize: 13)),
            Text("   FACULTY :", style: TextStyle(height: 2.0, fontSize: 13)),
            Text("   PROGRAM :", style: TextStyle(height: 2.0, fontSize: 13)),
            Text("   STATUS :", style: TextStyle(height: 2.0, fontSize: 13)),
          ],
        ),
      ],
    );
  }

  Widget colbioprofile() {
    return Row(
      children: <Widget>[
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text("", style: TextStyle(height: 2.0, fontSize: 14)),
            Text("63160065", style: TextStyle(height: 2.7, fontSize: 14)),
            Text("Keetapat Chomrasri",
                style: TextStyle(height: 2.0, fontSize: 14)),
            Text("NAN",
                style: TextStyle(height: 2.0, fontSize: 14)),
            Text("Informatics", style: TextStyle(height: 2.0, fontSize: 14)),
            Text("(CS)Computer Science",
                style: TextStyle(height: 2.0, fontSize: 14)),
            Text("Study", style: TextStyle(height: 2.0, fontSize: 14)),
          ],
        ),
      ],
    );
  }
}
