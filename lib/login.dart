import 'package:example_template/home_page.dart';
import 'package:flutter/material.dart';

class LoginPage extends StatelessWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: ConstrainedBox(
        constraints: const BoxConstraints(maxWidth: 300.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            //const SizedBox(height: 50.0),
            const Spacer(flex:5),

            //const SizedBox(height: 100.0),
            const Spacer(flex:10),
            const TextField(decoration: InputDecoration(labelText: 'Username')),
            const TextField(decoration: InputDecoration(labelText: 'Password')),
            const Spacer(flex:3),
            //const SizedBox(height: 30.0),
            ElevatedButton(onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => const Bottom()),
              );
            }, child: const Text('Login')),
            const Spacer(flex:20),
          ],
        ),
      ),
    );
  }
}
